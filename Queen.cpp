// Queen.cpp

#include "Queen.h"

bool Queen::legal_move_shape(std::pair<char,char> start,
			     std::pair<char, char> end) const {

  int row_dif = end.second - start.second;
  int col_dif = end.first - start.first;
  // Check for validity
  if(row_dif == 0 && col_dif == 0) {
    return false; // end position is start position
  }
  else if((row_dif == 0 && col_dif != 0) || (row_dif != 0 && col_dif == 0)) { 
    return true; // queen moving vertically or horizontally
  }
  else if(row_dif == col_dif || row_dif == -col_dif) {
    return true; // queen moving diagonally
  }
  else {
    return false;
  }

}
