//Bishop.cpp

#include "Bishop.h"

// Difine legal movement
bool Bishop::legal_move_shape(std::pair<char,char> start,
			      std::pair<char,char> end) const {

  //Find horizontal and vertical movement
  int row_dif = end.second-start.second;
  int col_dif = end.first-start.first;

  // Bishop  can only move diagonally, so the two movements must be the same or opposite sign and cannot be zero
  if (row_dif == 0 || col_dif == 0) {
    return false;
  }
  else if (row_dif == col_dif || row_dif == -1*col_dif) {
    return true;
  }
  else {
    return false;
  }
}
