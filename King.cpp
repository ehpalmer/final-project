// King.cpp

#include "King.h"

bool King::legal_move_shape(std::pair<char,char> start,
			    std::pair<char,char> end) const {

  int row_dif = end.second - start.second;
  int col_dif = end.first - start.first;

  // Check for validity
  if(row_dif == 0 && col_dif == 0) {
    return false; // end position is start postion
  }
  else if((row_dif >= -1 && row_dif <= 1) && (col_dif >= -1 && col_dif <= 1)) {
    return true; // moved by one square horizontally, vertically, or diagonally
  }
  else {
    return false; // moved by more than one square in any direction         
  }

}
