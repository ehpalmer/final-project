// Rook.cpp

#include "Rook.h"

bool Rook::legal_move_shape(std::pair<char, char> start,
			    std::pair<char,char> end) const {

  int row_dif = end.second-start.second;
  int col_dif = end.first-start.first;

  // Check for validity
  if((row_dif != 0) ^ (col_dif != 0)) {
    return true;
  }
  else {
    return false;
  }

}
