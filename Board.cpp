#include <iostream>
#include <utility>
#include <map>
#ifndef _WIN32
#include "Terminal.h"
#endif // !_WIN32
#include "Board.h"
#include "CreatePiece.h"


/////////////////////////////////////
// DO NOT MODIFY THIS FUNCTION!!!! //
/////////////////////////////////////
Board::Board( void ){}

const Piece* Board::operator()( std::pair< char , char > position ) const
{
  //Checks to see if there is a piece at that position and returns a pointer to the piece if there is one
  if(_occ.find(position) != _occ.end()) {
    return _occ.at(position);
  }
  //If there is none returns null
  else {
    return NULL;
  }
}

bool Board::add_piece( std::pair< char , char > position , char piece_designator )
{
  //Checks to make sure that the position is valid
  if (std::get<0>(position) < 'A' || std::get<0>(position) > 'H' || std::get<1>(position) < '1' || std::get<1>(position) > '8') {
    return false;
  }
  //Checks to see if the piece is valid
  else if (piece_designator != 'k' && piece_designator != 'K' && piece_designator != 'r' && piece_designator != 'R' && piece_designator != 'Q' && piece_designator != 'q' && piece_designator != 'B' && piece_designator != 'b' && piece_designator != 'N' && piece_designator != 'n' && piece_designator != 'P' && piece_designator != 'p') {
    return false;
  }
  //Checks to see if the position is already occupied
  else if (_occ.find(position) != _occ.end()) {
    return false;
  }
  //If none of the previoous are true, creates a piece at that position
  else {
    _occ[ position ] = create_piece( piece_designator );
    return true;
  }
}

// Remove a piece from the board
bool Board::remove_piece(std::pair<char,char> position) {
  // Make sure there is a piece at the position
  if(_occ.find(position) == _occ.end()) { return false; }
  delete _occ[position];
  _occ.erase(position);
  return true;
}

bool Board::has_valid_kings( void ) const
{
  //Initializes counters for kings
  int black_king = 0;
  int white_king = 0;
  //Checks map and counts number of black and white kings
  for (std::map<std::pair<char, char>,Piece*>::const_iterator it = _occ.cbegin();
       it != _occ.cend(); ++it) {
    if (it->second->to_ascii() == 'K') {
      white_king++;
    }
    else if (it->second->to_ascii() == 'k') {
      black_king++;
    }
  }
  //If there is only one of each king, returns true
  if (black_king == 1 && white_king == 1) {
    return true;
  }
  else {
    return false;
  }
}

// Change a piece's position in the board
bool Board::change_piece_loc(std::pair < char, char> start, std::pair <char, char> end) {
  // Make sure the piece is already on the board
  if(_occ.find(end) != _occ.end()) {
    // Make sure there is not the same color piece at the end
    if(_occ[end]->is_white() == _occ[start] -> is_white()) {
      return false;
    }
    else {
      delete _occ[end];
      _occ.erase(end);}
  }
  _occ[end] = _occ.at(start);
  _occ.erase(start); 
  return true; 
}

// Promotes a pawn
bool Board::promote_pawn(std::pair<char,char> position, bool is_white) {
  // Make sure piece exists at position
  if(_occ.find(position) == _occ.end()) {
    return false;}
  // Make sure the piece is a pawn
  if(_occ[position]->to_ascii() != 'P' && _occ[position]->to_ascii() != 'p') {
    return false;}

  // Delete the pawn and replace it with a queen
  delete _occ[position];
  _occ.erase(position);
  if(is_white) {  _occ[position] = create_piece('Q');}
  else { _occ[position] = create_piece('q'); }
  
  return true; 
}

void Board::display( void ) const
{
  //std::cout << "Show board. 1. " << std::endl;
  // Display board by printing it to stdout
  bool previous_black = true; // start on a light square
  Terminal::color_all(true, Terminal::WHITE, Terminal::BLACK); // set all text to white and default background to black
  for(char r = '8'+1 ; r >= '1' ; r-- ) {
    for(char c = 'A'-1 ; c <= 'H' ; c++ ) {
      //std::cout << "Show board. " << std::endl; 
      // Set background color
      if(r == '8'+1 || c == 'A'-1) {
	Terminal::color_bg(Terminal::DEFAULT_COLOR); // black for column and row labels
      }
      else if(previous_black) { // alternate between black and magenta background 
	Terminal::color_bg(Terminal::MAGENTA);
        if(c != 'H') previous_black = false;
      }
      else {
	Terminal::color_bg(Terminal::BLACK);
	if(c != 'H') previous_black = true;
      }
      // print piece or column labels

      //std::cout<<"Test 2" << std::endl; 
      std::pair< char , char > position = std::make_pair(c,r);
      //std::cout << "Test 4" << std::endl;
      
      //const Piece* piece =( std::pair< char , char >( c , r ) );
      Piece* piece = NULL; 
      if(_occ.find(position) != _occ.end()) {
	piece =  _occ.at(position);
      }

      //std::cout<<"test 3" << std::endl;
      if( r == '8' + 1 && c == 'A'-1) std::cout << "   ";
      else if(r == '8'+1) std::cout << ' '<< c << ' ';
      else if(c == 'A'-1) std::cout << ' ' << r << ' '; 
      else if( piece ) std::cout << ' ' << piece -> to_ascii() << ' ';
      else std::cout << "   ";
    }
    Terminal::color_bg(Terminal::DEFAULT_COLOR);
    std::cout << std::endl; 
  }
  Terminal::color_bg(Terminal::DEFAULT_COLOR);
}

/////////////////////////////////////
// DO NOT MODIFY THIS FUNCTION!!!! //
/////////////////////////////////////
std::ostream& operator << ( std::ostream& os , const Board& board )
{
	for( char r='8' ; r>='1' ; r-- )
	{
		for( char c='A' ; c<='H' ; c++ )
		{
			const Piece* piece = board( std::pair< char , char >( c , r ) );
			if( piece ) os << piece->to_ascii();
			else        os << '-';
		}
		os << std::endl;
	}
	return os;
}
