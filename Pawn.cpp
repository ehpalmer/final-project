//Pawn.cpp

#include "Pawn.h"

bool Pawn::legal_move_shape(std::pair<char,char> start,
			    std::pair<char,char> end) const {

  int col_dif = end.second - start.second;
  int row_dif = end.first - start.first;

  // Check for legal move of 1 vertically or 2 vertically if in a starting position
  if(row_dif == 0 && (col_dif == 1 || col_dif == -1)) {
    return true;
  }
  else if (row_dif == 0 && ((start.second == '7' && col_dif == -2) || (start.second == '2' && col_dif == 2))) {
    return true;
  }
  else {
    return false;
  }
}

bool Pawn::legal_capture_shape(std::pair<char,char> start,
			 std::pair<char,char> end) const {

  int row_dif = end.second - start.second;
  int col_dif = end.first - start.first;

  // Override capture since it is different than movement
  if ((col_dif == 1 || col_dif == -1) && (row_dif == 1 || row_dif == -1)) {
    return true;
  }
  else {
    return false;
  }
}
