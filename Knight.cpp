// Knight.cpp

#include "Knight.h"

bool Knight::legal_move_shape(std::pair<char,char>start,
			      std::pair<char,char>end) const {

  int row_dif = end.second - start.second;
  int col_dif = end.first - start.first;

  // Check for validity
  if(((row_dif == 2) || (row_dif == -2)) && ((col_dif == 1) || (col_dif == -1))) {
    return true;
  }
  else if(((row_dif == 1) || (row_dif == -1)) && ((col_dif == 2) || (col_dif == -2))) {
    return true;
  }
  else {
    return false;
  }

}
