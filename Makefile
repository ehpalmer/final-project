CC = g++
CONSERVATIVE_FLAGS = -std=c++11 -Wall -Wextra -pedantic
DEBUGGING_FLAGS = -g -O0
CFLAGS = $(CONSERVATIVE_FLAGS) $(DEBUGGING_FLAGS)


chess: main.o Board.o Chess.o CreatePiece.o Bishop.o King.o Knight.o Queen.o Pawn.o Rook.o Mystery.h
	$(CC) -o chess main.o Board.o Chess.o CreatePiece.o Bishop.o King.o Knight.o Queen.o Pawn.o Rook.o Mystery.h

Board.o: Board.cpp Board.h Mystery.h Bishop.o King.o Queen.o Pawn.o Knight.o Rook.o Piece.h CreatePiece.h Terminal.h
	$(CC) -c Board.cpp $(CFLAGS)

Chess.o: Chess.cpp Board.h Chess.h Piece.h Bishop.o King.o Pawn.o Queen.o Knight.o Mystery.h Rook.o
	$(CC) -c Chess.cpp $(CFLAGS)

CreatePiece.o: CreatePiece.cpp Bishop.o King.o Pawn.o Knight.o Queen.o  Mystery.h Rook.o Board.h Chess.h CreatePiece.h
	$(CC) -c CreatePiece.cpp $(CFLAGS)

main.o: main.cpp Bishop.o King.o Queen.o Pawn.o Knight.o Rook.o Mystery.h Board.h Chess.h CreatePiece.h
	$(CC) -c main.cpp $(CFLAGS)

Rook.o : Piece.h Rook.h Rook.cpp
	$(CC) -c Piece.h Rook.h Rook.cpp $(CFLAGS)

Queen.o : Queen.h Queen.cpp Piece.h
	$(CC) -c Queen.h Queen.cpp Piece.h $(CFLAGS)

Bishop.o : Bishop.h Bishop.cpp Piece.h
	$(CC) -c Bishop.h Bishop.cpp Piece.h $(CFLAGS)

King.o : King.h King.cpp Piece.h
	$(CC) -c King.h King.cpp Piece.h $(CFLAGS)

Knight.o : Knight.h Knight.cpp Piece.h
	$(CC) -c Knight.h Knight.cpp Piece.h $(CFLAGS)

Pawn.o : Pawn.h Pawn.cpp Piece.h
	$(CC) -c Pawn.h Pawn.cpp Piece.h $(CFLAGS)

.PHONY: clean all
clean:
	rm -f *.o chess
