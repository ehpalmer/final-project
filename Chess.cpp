#include "Chess.h"
#include <fstream>

/////////////////////////////////////
// DO NOT MODIFY THIS FUNCTION!!!! //
/////////////////////////////////////
Chess::Chess( void ) : _turn_white( true )
{
	// Add the pawns
	for( int i=0 ; i<8 ; i++ )
	{
		_board.add_piece( std::pair< char , char >( 'A'+i , '1'+1 ) , 'P' );
		_board.add_piece( std::pair< char , char >( 'A'+i , '1'+6 ) , 'p' );
	}

	// Add the rooks
	_board.add_piece( std::pair< char , char >( 'A'+0 , '1'+0 ) , 'R' );
	_board.add_piece( std::pair< char , char >( 'A'+7 , '1'+0 ) , 'R' );
	_board.add_piece( std::pair< char , char >( 'A'+0 , '1'+7 ) , 'r' );
	_board.add_piece( std::pair< char , char >( 'A'+7 , '1'+7 ) , 'r' );

	// Add the knights
	_board.add_piece( std::pair< char , char >( 'A'+1 , '1'+0 ) , 'N' );
	_board.add_piece( std::pair< char , char >( 'A'+6 , '1'+0 ) , 'N' );
	_board.add_piece( std::pair< char , char >( 'A'+1 , '1'+7 ) , 'n' );
	_board.add_piece( std::pair< char , char >( 'A'+6 , '1'+7 ) , 'n' );

	// Add the bishops
	_board.add_piece( std::pair< char , char >( 'A'+2 , '1'+0 ) , 'B' );
	_board.add_piece( std::pair< char , char >( 'A'+5 , '1'+0 ) , 'B' );
	_board.add_piece( std::pair< char , char >( 'A'+2 , '1'+7 ) , 'b' );
	_board.add_piece( std::pair< char , char >( 'A'+5 , '1'+7 ) , 'b' );

	// Add the kings and queens
	_board.add_piece( std::pair< char , char >( 'A'+3 , '1'+0 ) , 'Q' );
	_board.add_piece( std::pair< char , char >( 'A'+4 , '1'+0 ) , 'K' );
	_board.add_piece( std::pair< char , char >( 'A'+3 , '1'+7 ) , 'q' );
	_board.add_piece( std::pair< char , char >( 'A'+4 , '1'+7 ) , 'k' );
}

bool Chess::make_move( std::pair< char , char > start , std::pair< char , char > end )
{
  const Piece* piece_ptr = _board(start);
  
  // Check for a piece at the starting position
  if(piece_ptr == NULL) {return false;}

  
  // Check that the move is valid
  if(!piece_ptr->legal_move_shape(start,end) && !piece_ptr->legal_capture_shape(start,end)) {
    return false;
  }

  // Check that correct color piece is moving
  if(piece_ptr->is_white() != _turn_white) {
    return false; }

  // Check for moving over pieces if the piece is not a knight
  int row_dif = end.second - start.second;
  int col_dif = end.first - start.first;
  if(row_dif == col_dif || row_dif == 0 || col_dif == 0) {
    int move_mag = row_dif;
    /*if(row_dif > col_dif) {move_mag = row_dif;}
      else {move_mag = col_dif;}*/
    if (move_mag == 0) {
      move_mag = col_dif;
    }
    if(move_mag < 0) { move_mag = -1*move_mag; }
    if(move_mag == 0) {return false;}
	
    int unit_row = row_dif / move_mag;
    int unit_col = col_dif / move_mag;
    for(int i = 1; i < move_mag; ++i) {
      char newRow = start.second + unit_row*i;
      char newCol = start.first + unit_col*i;
      std::pair<char, char> current = std::make_pair(newCol, newRow);
      if(_board(current) != NULL) {return false;} 
    }
  }

  // Check for pawns moving backwards
  if ((piece_ptr->to_ascii() == 'P' && end.second < start.second ) || (piece_ptr->to_ascii() == 'p' && end.second > start.second)) {return false; }
  
  // Check for piece at end and color of piece
  const Piece* end_piece_ptr = _board(end);
  if(end_piece_ptr != NULL) {
    if(piece_ptr->is_white() == end_piece_ptr->is_white()) {
      return false;}
  }

  // Check for moving into check
  Board tmp_bd = _board;
  Chess tmp_ch(tmp_bd, _turn_white);
  tmp_ch._board.change_piece_loc(start,end);
  if(tmp_ch.in_check(_turn_white)) {return false;}

  // Make move
  _board.change_piece_loc(start, end);
  _turn_white = !_turn_white;
  
  // Check piece promotion
  if((end.second == '8' && piece_ptr->to_ascii() == 'P') || (end.second == '1' && piece_ptr->to_ascii() == 'p')) {
    _board.promote_pawn(end,piece_ptr->is_white()); 
  }
  return true;
}

bool Chess::in_check( bool white ) const
{
  // Find king position
  std::pair<char,char> king_loc = std::make_pair('0','0'); 
  for(char row = '1'; row <= '8'; row++) {
    for(char col = 'A'; col <= 'H'; col++) {
      std::pair<char,char> current = std::make_pair(col,row);
      const Piece* piece_ptr = _board(current);
      if(piece_ptr != NULL) {
	if((piece_ptr->to_ascii() == 'K' || piece_ptr->to_ascii() == 'k') && piece_ptr->is_white() == white) {
	  king_loc = current; 
	}
      }
    }
  }
  const Piece* king_ptr = _board(king_loc); 
  if(king_loc == std::make_pair('0','0')) {return false;}
  
  // Iterate over all pieces on the board to see if they put the king in check
  for(char row = '1'; row <= '8'; row++) {
    for(char col = 'A'; col <= 'H'; col++) {
      std::pair < char, char> current = std::make_pair(col,row);
      const Piece* piece_ptr = _board(current);
      
      // Doesn't check the position of the king                                
      if(king_loc.first == col && king_loc.second == row) {continue;}
      
      // Check for a piece at the starting position
      if(piece_ptr == NULL) {continue;}
      
      // Check that the move is valid 
      if(!piece_ptr->legal_capture_shape(current, king_loc)) {continue;}
      
      // Check for moving over pieces if the piece is not a knight
      int row_dif = king_loc.second - current.second;
      int col_dif = king_loc.first - current.first;
      if(row_dif == col_dif || row_dif == 0 || col_dif == 0) {
	int move_mag = row_dif;
	if (move_mag == 0) {
	  move_mag = col_dif;
	}
	if(move_mag < 0) { move_mag = -1*move_mag; }
	if(move_mag == 0) {break;}
	
	int unit_row = row_dif / move_mag;
	int unit_col = col_dif / move_mag;
	bool broken = false;
	for(int k = 1; k < move_mag; ++k) {
	  char newRow = current.second + unit_row*k;
	  char newCol = current.first + unit_col*k;
	  std::pair<char, char> between = std::make_pair(newCol, newRow);
	  if(_board(between) != nullptr) {broken = true; break;}
	}
	if(broken) {continue; }
      }
      
      // Make sure piece is of opposing color to the king                                 
      if(piece_ptr->is_white() != king_ptr->is_white()) {
	return true;}
    }
  }
  return false;
}

bool Chess::in_mate( bool white ) const
{
  // If we aren't in check, we aren't in checkmate
  if(!in_check(white)) {return false;}

  // Is there any move that will remove the check?
  for(char row = '1'; row <= '8'; row++) {
    for(char col = 'A'; col <= 'H'; col++) {
      std::pair<char,char> current = std::make_pair(col,row);
      const Piece* piece_ptr = _board(current);

      // If there is no piece, continue
      if(piece_ptr == NULL) {continue;}

      // If the piece is the opposing color, it can't be used to block
      if(piece_ptr->is_white() != white) {continue;}

      // Loop through the valid moves to see if it removes check
      for(char row_mv = '1'; row_mv <= '8'; row_mv++) {
	for(char col_mv = 'A'; col_mv <= 'H'; col_mv++) {
	  std::pair<char,char> mv = std::make_pair(col_mv,row_mv);

	  // Make a copy of the board so we can move pieces around
	  Board tmp_bd = _board;
	  Chess tmp_ch(tmp_bd, white);

	  if(!piece_ptr->legal_move_shape(current,mv) || (!piece_ptr->legal_capture_shape(current,mv) && _board(mv) != NULL)) {continue;}
	  else {
	    tmp_ch.make_move(current,mv);
	    if(!tmp_ch.in_check(white)) {return false;}
	  }
	}
      }
    }
  }
  return true;
}

bool Chess::in_stalemate( bool white ) const
{
  // If we are in check, we aren't in stalemate
  if(in_check(white)) {return false;}

  // Loop to find valid move
  for(char row = '1'; row <= '8'; row++) {
    for(char col = 'A'; col <= 'H'; col++) {
      std::pair<char,char> current = std::make_pair(col,row);
      const Piece* piece_ptr = _board(current);

      // If there is no piece, continue
      if(piece_ptr == NULL) {continue;}

      // If the piece is the opposing color, it can't be moved
      if(piece_ptr->is_white() != white) {continue;}

      // Loop through to find valid movek
      for(char row_mv = '1'; row_mv <= '8'; row_mv++) {
	for(char col_mv = 'A'; col_mv <= 'H'; col_mv++) {
	  std::pair<char,char> mv = std::make_pair(col_mv,row_mv);

	  // Make a copy of the board so we can move pieces around
	  Board tmp_bd = _board;
	  Chess tmp_ch(tmp_bd, white);

    
	  if(!piece_ptr->legal_move_shape(current,mv)) {continue;}
	  if(tmp_ch.make_move(current,mv)) {return false;}
	}
      }
    }
  }
  return true; 
}

// Add a piece to the board from the input stream
bool Chess::add_from_input(std::pair <char, char> pos, char piece) {
  if(_board.add_piece( pos , piece)) {return true;}
  return false;
}

// Clear the board
void Chess::clear_board(void) {
  for(char row = '1'; row <= '8'; row++) {
    for(char col = 'A'; col <= 'H'; col++) {
      std::pair<char,char> position = std::make_pair(col,row);
      _board.remove_piece(position);
    }
  }
}

/////////////////////////////////////
// DO NOT MODIFY THIS FUNCTION!!!! //
/////////////////////////////////////
std::ostream& operator << ( std::ostream& os , const Chess& chess )
{
	// Write the board out and then either the character 'w' or the character 'b', depending on whose turn it is
	return os << chess.board() << ( chess.turn_white() ? 'w' : 'b' );
}

std::istream& operator >> ( std::istream& is , Chess& chess )
{
  chess.clear_board();
  for (char r='8' ; r>='1' ; r--) {
    for (char c='A' ; c<='H' ; c++) {
      std::pair<char, char> pos =  std::make_pair(c,r);
      char piece;
      is >> piece;
      if ( piece != '-' ) {
       	//chess.board().add_piece(pos,piece);
	chess.add_from_input(pos,piece);
      }
    }
  }
  char turn;
  is >> turn;
  if(turn == 'b') {chess.set_turn(false);}
  else {chess.set_turn(true);}
  return is; 
}
